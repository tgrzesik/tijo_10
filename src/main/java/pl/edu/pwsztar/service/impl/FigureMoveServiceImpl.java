package pl.edu.pwsztar.service.impl;

import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.FigureMoveService;

import java.util.Arrays;
import java.util.List;

@Service
public class FigureMoveServiceImpl implements FigureMoveService {

    @Override
    public boolean bishopMove(FigureMoveDto figureMoveDto) {
        int startX;
        int startY;
        int destinationX;
        int destinationY;

        List<String> startPosition = Arrays.asList(figureMoveDto.getStart().split("_"));
        List<String> destinationPosition = Arrays.asList(figureMoveDto.getDestination().split("_"));

        startX = Integer.parseInt(startPosition.get(1));
        destinationX = Integer.parseInt(destinationPosition.get(1));
        startY = columnNameToInteger(startPosition.get(0));
        destinationY = columnNameToInteger(destinationPosition.get(0));

        return (Math.abs(startX - destinationX) == Math.abs(startY - destinationY));
    }

    private int columnNameToInteger(String columnName){
        return columnName.charAt(0) - 96;
    }
}
